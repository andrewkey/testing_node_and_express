
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      return Promise.all([
        // Inserts seed entries
        knex('users').insert({username: 'andrew', email: 'andrew.key@redpanda.ai'}),
        knex('users').insert({username: 'anna', email: 'anna.peczeli@redpanda.ai'})
      ]);
    });
};
