process.env.NODE_ENV = 'test';

const chai = require('chai');
const should = chai.should();
const fs = require('fs');
const path = require('path');

const usersController = require('../../src/server/controllers/users');

describe('controllers : users', () => {
  describe('filterByYear()', () => {
    const userArray = [
      { id: 1, username: 'andrew', email: 'andrew.key@redpanda.ai', created_at: '2018-06-05T13:00:00.000Z' },
      { id: 2, username: 'marq', email: 'marq.short@zumepizza.com', created_at: '2017-06-05T13:00:02.000Z' },
      { id: 3, username: 'ahmed', email: 'ahmed.farooki@zumepizza.com', created_at: '2010-06-05T13:00:00.000Z' }
    ];
    it('should return all users create on or after (>=) specified year', (done) => {
      usersController.filterByYear(userArray, 2015, (err, total) => {
        should.not.exist(err);
        total.length.should.equal(2);
        done();
      })
    });
  })

  describe('filterByYear() with helper', () => {
    it('should return all users created on or after (>=) specified year',
      (done) => {
        const testDataFile = path.join(
          __dirname, '..', 'test.data.json');
        fs.readFile(testDataFile, 'utf8', (err, data) => {
          usersController.filterByYear(
            JSON.parse(data), 2015, (err, total) => {
              should.not.exist(err);
              total.length.should.eql(5);
              done();
            });
        });
      });
  });

});